template<typename type, type f, type s> constexpr type n_c_d() {
	if constexpr (f == 0)
		return s;
	else if constexpr (s == 0)
		return f;
	else if constexpr (f % 2 == 0 && s % 2 == 0)
		return n_c_d<type, f / 2, s / 2>() * 2;
	else if constexpr (f % 2 == 1 && s % 2 == 0)
		return n_c_d<type, f, s / 2>();
	else if constexpr (f % 2 == 0 && s % 2 == 1)
		return n_c_d<type, f/2, s>();
	else if constexpr (f & 2 == 1 && s % 2 == 1 && f < s)
		return n_c_d<type, (s - f) / 2, f>();
	else if constexpr (f & 2 == 1 && s % 2 == 1 && s < f)
		return n_c_d<type, (f - s) / 2, s>();
	return 1;
}

template<typename type, type a> constexpr type abs() {
	if constexpr (a < 0) return -a;
	else return a;
}

template<typename type, type a> constexpr type get_sign() {
	if constexpr (a < 0) return -1;
	else return 1;
}

template<typename type, type f, type s, short int comp> constexpr type get_coprime() {
	static_assert(comp == 0 || comp == 1);

	constexpr type ncd = n_c_d<type, f, s>();
	if constexpr (ncd == 1) {
		if constexpr (comp == 0) return f;
		if constexpr (comp == 1) return s;
	}
	else return get_coprime<type, f / ncd, s / ncd, comp>();
}

template<typename type, type _num, type _den> struct frac {

	using value_type = type;

	static_assert(_den != 0);

	static constexpr type sign = get_sign<type, _num>() * get_sign<type, _den>();
	static constexpr type num = get_coprime<type, abs<type, _num>(), abs<type, _den>(), 0>();
	static constexpr type den = get_coprime<type, abs<type, _num>(), abs<type, _den>(), 1>();

	static constexpr double eval = (double)num / (double)den * (double)sign;
};

template<class f1, class f2> struct arithm {
	using type = typename f1::value_type;

	using sum = frac<type, f1::sign*f1::num*f2::den + f2::sign*f2::num*f1::den, f1::den*f2::den>;
	using res = frac<type, f1::sign*f1::num*f2::den - f2::sign*f2::num*f1::den, f1::den*f2::den>;
	using mul = frac<type, f1::sign*f2::sign*f1::num*f2::num, f2::den * f1::den>;
	using div = frac<type, f1::sign*f2::sign*f1::num*f2::den, f1::den * f2::num>;
};


template<size_t i, class ArgF = void, class... Args> struct get_elem {
	using result = typename get_elem<i - 1, Args...>::result;
};

template<class ArgF, class...Args> struct get_elem<0,ArgF,Args...> {
	using result = ArgF;
};

template<class ArgF = void, class... Args> struct get_size {
	static constexpr size_t result = get_size<Args...>::result + 1;
};

template<> struct get_size<> {
	static constexpr size_t result = 0;
};

template<class ArgFirst = void, class... Args> struct param_pack{
	using get_first = ArgFirst;
	using get_rest = param_pack<Args...>;

	static constexpr size_t size = get_size<Args>::result + 1;
};

template<class ArgFirst> struct param_pack<ArgFirst> {
	using get_first = ArgFirst;
	using get_rest = void;

	static constexpr size_t size = 1;
};

template<> struct param_pack<void> {
	using get_first = void;
	using get_rest = void;
	static constexpr size_t size = 0;
};

template<class ArgFirst, class... Args> struct param_pack<ArgFirst, param_pack<Args...>> {
	using get_first = ArgFirst;
	using get_rest = param_pack<Args...>;

	static constexpr long size_t = 1 + get_size<Args...>::result;
};

template<size_t i, class ArgF, class... Args> struct get_elem<i, param_pack<ArgF, Args...>> {
	using result = typename get_elem<i - 1, param_pack<Args...>>::result;
};

template<class ArgF, class... Args> struct get_elem<0, param_pack<ArgF, Args...>> {
	using result = ArgF;
};

template<size_t row, size_t rowsize, size_t counter, bool cond, class... Args> struct get_row;

template<size_t row, size_t rowsize, size_t counter, class... Args> struct get_row<row, rowsize, counter, false, Args...> {
	using result = typename get_row<row, rowsize, counter + 1, row*rowsize == (counter+1), typename param_pack<Args...>::get_rest>::result;
};

template<size_t row, size_t rowsize, size_t counter, class... Args> struct get_row<row, rowsize, counter, false, param_pack<Args...>> {
	using result = typename get_row<row, rowsize, counter + 1, row*rowsize == (counter+1), typename param_pack<Args...>::get_rest>::result;
};

template<size_t row, size_t rowsize, size_t counter, class... Args> struct get_row<row, rowsize, counter, true, Args...> {
	using result = param_pack<Args...>;
};

template<size_t row, size_t rowsize, size_t counter, class... Args> struct get_row<row, rowsize, counter, true, param_pack<Args...>> {
	using result = param_pack<Args...>;
};

template<class... Args> struct vector {
	static constexpr size_t size = get_size<Args...>::result;

	using values = param_pack<Args...>;

	template<size_t i> using get = typename get_elem<i, Args...>::result;

	template<size_t n = 0> static void eval_and_print() {
		std::cout << get_elem<n, values>::result::eval << " ";
	}

	template<> static void eval_and_print<size>() {
		std::cout << "\n";
	}

	template<size_t n> static constexpr double get_eval() {
		return get_elem<n, Args...>::result::eval;
	}
};

template<class... Args> struct vector<param_pack<Args...>> {
	static constexpr size_t size = get_size<Args...>::result;

	using values = param_pack<Args...>;

	template<size_t n = 0> static void eval_and_print() {
		std::cout << get_elem<n, Args...>::result::eval << " ";
		eval_and_print<n + 1>();
	}

	template<> static void eval_and_print<size>() {
		std::cout << "\n";
	}

	template<size_t n> static constexpr double get_eval() {
		return get_elem<n, Args...>::result::eval;
	}
};

template<class V1, class V2, int size = V1::size> struct scalar_mult {
	using Args1 = typename V1::values;
	using Args2 = typename V2::values;

	using result = typename arithm<typename arithm<typename Args1::get_first, typename Args2::get_first>::mul, typename scalar_mult<vector<typename Args1::get_rest>, vector< typename Args2::get_rest>>::result>::sum;
};

template<class V1, class V2> struct scalar_mult<V1, V2, 1> {
	using Args1 = typename V1::values;
	using Args2 = typename V2::values;

	using result = typename arithm<typename Args1::get_first, typename Args2::get_first>::mul;
};

template<class... Args> struct matrix {
	static constexpr size_t size = get_size<Args...>::result;

	using values = param_pack<Args...>;
};

template<class... Args> struct matrix<param_pack<Args...>> {
	static constexpr size_t size = get_size<Args...>::result;

	using values = param_pack<Args...>;

	template<size_t i, size_t j, size_t rowsize> using get = typename get_elem<rowsize*i + j, Args...>::result;
	template<size_t i, size_t rowsize> using get_row = vector<typename get_row<i, rowsize, 0, i == 0, Args...>::result>;
};

template<class V, class M> struct mult_vec_mat{
	static_assert(V::size*V::size == M::size);

	using V_par_pack = typename V::values;
	using M_par_pack = typename M::values;

	template<int counter> struct raw {
		using row = typename get_row<counter, V::size, 0, counter == 0, M_par_pack>::result;

		using result = param_pack<typename scalar_mult<V, vector<row>>::result, typename raw<counter + 1>::result>;
	};

	template<> struct raw<V::size> {
		using result = void;
	};

	using result = vector<typename raw<0>::result>;
};